from django.http import Http404
from functools import wraps
from django.utils.decorators import available_attrs
from django.conf import settings

def https_only(function=None):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """

    def is_https(function):
        @wraps(function, assigned=available_attrs(function))
        def _wrapped_view(request, *args, **kwargs):
            if settings.HTTPS_ONLY_LOGIN and not request.is_secure():
                raise Http404
            else:
                return function(request, *args, **kwargs)

        return _wrapped_view

    if function:
        return is_https(function)
    raise Http404
