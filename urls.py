# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.conf import settings
from django.core.urlresolvers import reverse
# from oscar.app import application
# Uncomment the next two lines to enable the admin:

from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from django.contrib import admin
from django.views.generic import TemplateView
from da_login.views import *

admin.autodiscover()

urlpatterns = [
    # url(r'^login$', 'da_login.views.login', name='login'),
    url(r'^login/$', login, name='login'),
    url(r'^registration/$', register, name='registration'),
    url(r'^logout/$', logout, name='logout'),
    url(r'^profile/edit/$', edit_profile, name='edit_profile'),

    url(r'^profile/$', profile, name='profile'),

    # url(r'^inside/profile/login/$', login, name='inside_login'),
    # url(r'^inside/profile/register/$', register, name='inside_register'),
    # url(r'^inside/profile/logout/$', logout, name='inside_logout'),
    # url(r'^inside/profile/edit/$', edit_profile, name='inside_edit_profile'),
    #
    # url(r'^inside/profile/$', profile, name='inside_profile'),
    #
    # url(r'^profile_change_avatar$', 'da_login.views.profile_change_avatar', name='profile_change_avatar'),
    #
    # url(r'^profile/(?P<username>[a-zA-Z0-9\-_\.\/]+)$', 'da_login.views.another_profile', name='another_profile'),

    # url(r'^node_subscribe$', 'da_login.views.node_subscribe', name='node_subscribe'),
    url(r'^facebook_oauth/$', facebook_oauth, name='facebook_oauth'),

]
